<?php

session_start();

if($_SESSION===null||$_SESSION===''){
    
    echo("no hay sesion");
    header("Status: 301 Moved Permanently");
    header("Location: ../index.php");
    exit;  

}
// if(isset($_POST["ing_desc"]))
// {
// 	print_r($_POST);
// $suma=$_POST["ing_desc"]/100;
// }
// else
// {
// 	$suma=1;
// }



?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FoodLord Delivery Menu</title>
  	<!-- Facebook and Twitter  links urls -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css son para las animaciones del texto o botones -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon sirve par atipos de iconos face twit-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons para iconos especificos sonido pantalla de carga-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap para el centreado -->
	<link rel="stylesheet" href="css/bootstrap.css">

		<!-- Magnific PopupMagnific Popup es una secuencia de comandos lightbox y dialog responsive con foco en el rendimiento y una mejor experiencia para el usuario con cualquier dispositivo 
 usado para las imagenes en columnas-->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker hora-->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  para las imagenes slider etc-->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>


	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a>FoodLord Delivery <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
					<ul >
                        <li>
<a href="">User: <strong><?php echo $_SESSION['nombre'].' '. $_SESSION['apellido_paterno'];?></strong> </a>
</li>
                          
                             
                        </ul>
						<li><a class="cursive-font"><a href="../controlador/cerrar_session.php" >Cerrar Session</a></li>
						<li class="has-dropdown">
							<a href="pagarform.php">Realizar Compra</a>

					</ul>	
				</div>
			</div>
			
		</div>
	</nav>
	
	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/fondocomida1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					

					<div class="row row-mt-15em">
						<div class="col-md-12 mt-text animate-box" data-animate-effect="fadeInUp">
						
							<h1 class="cursive-font">Pruebalo Todo 7u7s</h1>
							<!-- aqui esta  mi dessc -->
							<form action="#" method="post">

<p>Aplicar descuento del: 
  5%: <input type="radio" name="des" value="5" />. .
  10%: <input type="radio" name="des" value="10" />. .
  20%: <input type="radio" name="des" value="20" />. .
  30%: <input type="radio" name="des" value="30" />
  40%: <input type="radio" name="des" value="40" />
  50%: <input type="radio" name="des" value="50" />
  60%: <input type="radio" name="des" value="60" />
  70%: <input type="radio" name="des" value="70" />
  80%: <input type="radio" name="des" value="80" />
  90%: <input type="radio" name="des" value="90" />
</p>
<p><input type="submit" value="Ver resultado" /></p>
</form>
<?php  
function rebajas($base,$dto=0) {
         $ahorro = ($base*$dto)/100;
         $final = $base-$ahorro;
         return array($ahorro, $final);
         }
$precio=0;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);
// echo "Precio inicial: $precio. <br/>";
// echo "artículo rebajado en un $descuento%. <br/>";
// echo "Este artículo está rebajado en $rebaja €. <br/>";
// echo "Precio final del artículo: $precioFinal €.";
?>

						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 class="cursive-font primary-color">Nuestra comida</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/img_1.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/img_1.jpg" alt="Image" class="img-responsive">
						</figure>
					</a>
						<div class="fh5co-text">
					
							<a><h2>Panqueques</h2></a>
							
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=19.5;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							<p><span class="price cursive-font" ></span></p>
						</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/img_2.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/img_2.jpg" alt="Image" class="img-responsive">
						</figure>
					</a>
						<div class="fh5co-text">
							<a href="../controlador/carrito_controlador.php"><h2>Hamburguesa</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=20.99;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/img_3.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/img_3.jpg" alt="Image" class="img-responsive">
						</figure>
					</a>
						<div class="fh5co-text">
							<a href="../controlador/carrito_controlador.php"><h2>Choripan</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=8.99;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							

						</div>
				</div>


				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/img_4.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/img_4.jpg" alt="Image" class="img-responsive">
						</figure>
					</a>
						<div class="fh5co-text">
							<a href="../controlador/carrito_controlador.php"><h2>costillas</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=55;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a   href="images/img_5.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img  href="pagarform.php" src="images/img_5.jpg" alt="Image" class="img-responsive">
						</figure>
					</a>
						<div class="fh5co-text">
							<h2><a href="../controlador/carrito_controlador.php">Pizza</a></h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=23.10;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
							
						</div>
					
				</div>
				

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/img_6.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/img_6.jpg" alt="Image" class="img-responsive">
						</figure>
					</a>
						<div class="fh5co-text">
							<a href="../controlador/carrito_controlador.php"><h2>Postre Chocolate</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=5.5;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
				</div>

			</div>
		</div>
	</div>

		<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/c.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/c.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>Moccachino</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=2.59;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/d.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/d.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>tacos mexicanos</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=26.59;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/e.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/e.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>dedos de queso</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=47.59;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/f.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/f.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>pique macho</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=99;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/g.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/g.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>sillpancho</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=12;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/h.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/h.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>alitas</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=32;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/i.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/i.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>pastas</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=55;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/j.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/j.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>postre flan</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<?php
							$precio=3;
$descuento=$_POST['des'];			 
list($rebaja,$precioFinal)=rebajas($precio,$descuento);

echo "<p><span class='price cursive-font' > $precioFinal $.</span></p>";
?>
							
						</div>
					</a>
				</div>

					<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/k.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/k.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
						<a href="../controlador/carrito_controlador.php"><h2>bife</h2></a>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$15.00</span></p>
						</div>
					</a>
				</div>

				

			</div>
		</div>
	</div>

	<div id="gtco-counter" class="gtco-section">
		<div class="gtco-container">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 class="cursive-font primary-color">
Nuestras Empresas</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/empresa1.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/empresa1.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Tuesday</h2>
							<p>Hamburguesas especiales..</p>
							<p><span class="price cursive-font">20%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/a.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/a.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Fridays</h2>
							<p>alitas con jack daniels..</p>
							<p><span class="price cursive-font">50%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/b.png" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/b.png" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Casa del camba</h2>
							<p>su comida a buen precio..</p>
							<p><span class="price cursive-font">sin descuentos</span></p>

						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/em1.png" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/em1.png" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>burguer king</h2>
							<p>Hamburguesas especiales..</p>
							<p><span class="price cursive-font">20%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/em2.jpeg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/em2.jpeg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>elis</h2>
							<p>pizzas..</p>
							<p><span class="price cursive-font">20%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/em4.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/em4.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>wrap and roll</h2>
							<p>kebhaps..</p>
							<p><span class="price cursive-font">20%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/em5.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/em5.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>flavor</h2>
							<p>helados..</p>
							<p><span class="price cursive-font">20%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="images/em6.jpeg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="images/em6.jpeg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>dumbo</h2>
							<p>..</p>
							<p><span class="price cursive-font">70%</span></p>
						</div>
					</a>
				</div>

		</div>
	</div>
	

	<footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row row-pb-md">

				

				
				<div class="col-md-12 text-center">
					<div class="gtco-widget">
						<h3>Get In Touch</h3>
						<ul class="gtco-quick-contact">
							<li><a href="#"><i class="icon-phone"></i> +1 234 567 890</a></li>
						
							<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>
						</ul>
					</div>
					<div class="gtco-widget">
						<h3>Get Social</h3>
						<ul class="gtco-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>

		

			</div>

			

		</div>
	</footer>
	<!-- </div> -->

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>


