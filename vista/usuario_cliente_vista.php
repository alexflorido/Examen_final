<!DOCTYPE HTML>

<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><a href="index.php">FoodLord Company <em>.</em></a> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

  <!--estos api son para las letras ,tipo de letra-->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css son para las animaciones del texto o botones -->
	<link rel="stylesheet" href="vista/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="vista/css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="vista/css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="vista/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="vista/css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="vista/css/bootstrap-datetimepicker.min.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="vista/css/owl.carousel.min.css">
	<link rel="stylesheet" href="vista/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="vista/css/style.css">

	<!-- Modernizr JS -->
	<script src="vista/js/modernizr-2.6.2.min.js"></script>
	
	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.php">FoodLord Company<em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
						
						
                        <li class="btn-cta"><a href="index.php"><span>Registrate</span></a></li>
                       
						
					</ul>	
				</div>
			</div>
			
		</div>
	</nav>
	<div class="modal fade1" id="reg_modal1" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Carrito de compras</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		<form action="process_insert_user.php" method="post">                   
  
				
		<section id="container-carrito-compras">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div id="carrito-compras-tienda"></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <p class="text-center" style="font-size: 80px;">
                    <i class="fa fa-shopping-cart"></i>
                </p>
                <p class="text-center">
                    <a href="pedido.php" class="btn btn-success btn-block"><i class="fa fa-dollar"></i>   Confirmar pedido</a>
                    <a href="process/vaciarcarrito.php" class="btn btn-danger btn-block"><i class="fa fa-trash"></i>   Vaciar carrito</a> 
                </p>
            </div>
        </div>
    </div>
	
	</section>
						  </form>  
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	</div>
  </div>
	
	<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/fondocomida1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<h1 class="cursive-font">Puedes Probarlo Todo!!!!</h1>	
						</div>
						<div class="col-md-4 col-md-push-1 animate-box" data-animate-effect="fadeInRight">
							<div class="form-wrap">
								<div class="tab">
									
									<div class="tab-content">
										<div class="tab-content-inner active" data-content="signup">
											<h3 >logeate</a></h3>
											<form action="controlador/log_in_controlador.php" method="POST">
												<div class="row form-group">
													<div class="col-md-12">
														<label for="activities" >Nombre de Usuario</label>
														<input type="text"  id="user" name="user" class="form-control" placeholder="&#128272; Usuario">
													</div>
												</div>
												<div class="row form-group">
													<div class="col-md-12">
														<label for="date-start"  >Contraseña</label>
														<input type="password" id="clave" name="clave" class="form-control" placeholder="&#128272; Contraseña">
													</div>
												</div>
												<li><a class="cursive-font"><a href="#"data-toggle="modal" data-target="#reg_modal" href="signup.php">Registrarse</a></li>
												</div>

												<div class="row form-group">
													<div class="col-md-12">
														<input type="submit" class="btn btn-primary btn-block" value="Ingresa Ahora">
													</div>
												</div>
											</form>	
										</div>

										
									</div>
								</div>
							</div>
						</div>
					</div>
							
					
				</div>
			</div>
		</div>
	</header>
<!-- Modal -->
<div class="modal fade" id="reg_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Registrar Usuario</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		<form action="controlador/insertar_usuario_controlador.php" method="post">                   
  
				
			 
							  <label>Nombre</label>
							  <input type="text"name="txt_firstname"class="form-control"placeholder="Nombre...">
							   
							  <label>Apellidos</label>
							  <input type="text"name="txt_lastname"class="form-control"placeholder="Apellidos...">
				  
							  <label>Direccion</label>
							  <input type="text"name="txt_direccion"class="form-control"placeholder="Direccion...">

							  <label>Nit</label>
							  <input type="text"name="txt_nit"class="form-control"placeholder="Nit...">

							  <label>Email</label>
							  <input type="text"name="txt_email"class="form-control"placeholder="Nombre...">
				  
							  <label>Nombre de Usuario</label>
							  <input type="text"name="txt_nusername"class="form-control"placeholder="Nombre de Usuario...">
				  
							  <label>Contraseña</label>
							 <input type="password"name="txtnpasswd"class="form-control"placeholder="Contraseña...">        
						                  
  
							
						
							 <input class="btn btn-primary btn-block" type="submit" value="Save">  
						  </form>  
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	</div>
  </div>
	
	
	<div class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 class="cursive-font primary-color">
Platos populares</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/img_1.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/img_1.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>panqueques</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$19.15</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/img_2.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/img_2.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>hamburguesa tocino</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$20.99</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/img_3.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/img_3.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Choripan</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$8.99</span></p>

						</div>
					</a>
				</div>


				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/img_4.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/img_4.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Costillitas</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$12.99</span></p>
						</div>
					</a>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/img_5.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/img_5.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>pizza</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$23.10</span></p>
						</div>
					</a>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/img_6.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/img_6.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Postre Chocolate</h2>
							<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							<p><span class="price cursive-font">$5.59</span></p>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>
	
	<div class="gtco-cover gtco-cover-sm" style="background-image: url(vista/images/img_bg_1.jpg)"  data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container text-center">
			<div class="display-t">
				<div class="display-tc">
					<h1>&ldquo; 
¡Su alta calidad de servicio me hace volver una y otra vez!&rdquo;</h1>
				
			</div>
		</div>
	</div>

	<div id="gtco-counter" class="gtco-section">
		<div class="gtco-container">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 class="cursive-font primary-color">
Nuestras Empresas</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">

				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/empresa1.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/empresa1.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Tuesday</h2>
							<p>Hamburguesas especiales..</p>
							<p><span class="price cursive-font">20%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/a.jpg" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/a.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Fridays</h2>
							<p>Este mes con nuestras alitas con jack daniels..</p>
							<p><span class="price cursive-font">50%</span></p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="vista/images/b.png" class="fh5co-card-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="vista/images/b.png" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Casa del camba</h2>
							<p>su comida a buen precio..</p>
							<p><span class="price cursive-font">sin descuentos</span></p>

						</div>
					</a>
				</div>
		</div>
	</div>

	

	<div id="gtco-subscribe">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2 class="cursive-font">Subscribete</h2>
			
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<form class="form-inline">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="email" class="sr-only">Email</label>
								<input type="email" class="form-control" id="email" placeholder="Your Email">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<button type="submit" class="btn btn-default btn-block">Subscribete</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row row-pb-md">

				

				
				<div class="col-md-12 text-center">
					
					<div class="gtco-widget">
						<h3>
Socializa</h3>
						<ul class="gtco-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>

			</div>

			

		</div>
	</footer>
	<!-- </div> -->

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
		<!-- jQuery -->
		<script src="vista/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="vista/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="vista/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="vista/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="vista/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="vista/js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="vista/js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="vista/js/jquery.magnific-popup.min.js"></script>
	<script src="vista/js/magnific-popup-options.js"></script>
	
	<script src="vista/js/moment.min.js"></script>
	<script src="vista/js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="vista/js/main.js"></script>

	</body>
</html>