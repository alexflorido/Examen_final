<?php
session_start();

$cart = array(
	array("product_name"=>"Pizza","product_quantity"=>"1","product_price"=>"23.10"),
//	array("product_name"=>"Producto 1","product_quantity"=>"1","product_price"=>"0.40"),
//	array("product_name"=>"Producto 1","product_quantity"=>"2","product_price"=>"0.40"),
	);

$_SESSION["cart"]=$cart;

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css son para las animaciones del texto o botones -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon sirve par atipos de iconos face twit-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons para iconos especificos sonido pantalla de carga-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap para el centreado -->
	<link rel="stylesheet" href="css/bootstrap.css">

		<!-- Magnific PopupMagnific Popup es una secuencia de comandos lightbox y dialog responsive con foco en el rendimiento y una mejor experiencia para el usuario con cualquier dispositivo 
 usado para las imagenes en columnas-->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker hora-->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  para las imagenes slider etc-->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>

</head>
<body>
<div class="gtco-loader"></div>
	
	<div id="page">

	
	<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.php">Tasty Food <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
						<li class="active"><a href="menu.php">Menu</a></li>
						<li class="has-dropdown">
							<a href="services.php">Services</a>
							<ul class="dropdown">
								<li><a href="#">codigos de descuentos</a></li>
								
							</ul>
						</li>
						<li><a href="contact.php">Contact</a></li>
						<li class="btn-cta"><a href="index.php"><span>Registrate</span></a></li>
					</ul>	
				</div>
			</div>
			
		</div>
		</div>
		</div>
	</nav>
<div class="container">
<div class="row">
<div class="col-md-12">

<h1>Productos por comprar</h1>

<?php if(count($cart)>0):?>
<table class="table table-bordered">
	<thead>
		<th>Cantidad</th>
		<th>Producto</th>
		<th>Precio</th>
		<th>Total</th>
	</thead>
<?php foreach($cart as $c):?>
	<tr>
		<td><?php echo $c["product_quantity"];?></td>
		<td><?php echo $c["product_name"];?></td>
		<td>$ <?php echo $c["product_price"];?></td>
		<td>$ <?php echo $c["product_quantity"]*$c["product_price"];?></td>
	</tr>
<?php endforeach; ?>
</table>
<a href="./process.php" class="btn btn-primary">Proceder a Pagar</a>
<a href="../controlador/confirmar_pedido_controlador.php" class="btn btn-primary">Confirmar pedido</a>
<?php endif; ?>
<br><br>

</div>
</div>
<footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row row-pb-md">

				

				
				<div class="col-md-12 text-center">
					<div class="gtco-widget">
						<h3>Get In Touch</h3>
						<ul class="gtco-quick-contact">
							<li><a href="#"><i class="icon-phone"></i> +591 60713463</a></li>
						
							<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>
						</ul>
					</div>
					<div class="gtco-widget">
						<h3>Get Social</h3>
						<ul class="gtco-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>

		

			</div>

			

		</div>
	</footer>
	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>

</body>
</html>