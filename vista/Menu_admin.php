<?php 
session_start();
if($_SESSION["id_admin"])
{

}
else
{
	header("location:../index.php");
}


?>
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>FoodLord Delivery</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
    
    <nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo">FoodLord Delivery<em>.</em></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
					<li><a class="cursive-font"><a href="#"data-toggle="modal" data-target="#reg_modal" href="signup.php">ADD COMPANY</a></li>
						<li class="has-dropdown">
						<li><a class="cursive-font"><a href="#"data-toggle="modal" data-target="#comida_modal" href="signup.php">ADD FOOD</a></li>
							
                            
                        </li>
                        <li><a class="cursive-font"><a href="#"data-toggle="modal" data-target="#repartidor_modal" href="signup.php">ADD Delivery</a></li>
						<li><a class="cursive-font"><a href="../controlador/cerrar_session.php" >Cerrar Session</a></li>
						
					</ul>	
				</div>
			</div>
			
		</div>
	</nav>
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(images/img_bg_3.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">

			</div>
		</div>
	</header>
<!--  //////////////////////////////////////////////////////////////////////////        -->
<!--   Modal Ingresar Empresa                                           -->
	<div class="modal fade" id="reg_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Registrar Empresa</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		<form action="../controlador/insertar_empresa_controlador.php" method="post">                   
  
				
			 
							  <label>Nombre Empresa</label>
							  <input type="text"name="nombre_empresa"class="form-control"placeholder="Nombre Empresa...">
							   
							  <label>Descripcion</label>
							  <input type="text"name="descripcion_empresa"class="form-control"placeholder="Descripcion...">
				  
  
							
						
							 <input class="btn btn-primary btn-block" type="submit" value="Save">  
						  </form>  
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	</div>
  </div>
<!--  End Modal Ingresar Empresa                                           -->
<!--  //////////////////////////////////////////////////////////////////////////        -->
<!--   Modal Ingresar Comida -->

<div class="modal fade" id="comida_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Registrar Menu</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		<form action="../controlador/insertar_menu_controlador.php" method="post">                   
  
				
			 
							  <label>Nombre Menu</label>
							  <input type="text"name="nombre_menu"class="form-control"placeholder="Nombre Empresa...">
							   
							  <label>Descripcion</label>
							  <input type="text"name="descripcion_menu"class="form-control"placeholder="Descripcion...">

							  <label>Precio</label>
							  <input type="text"name="precio_menu"class="form-control"placeholder="Precio...">



						
  
							
						
							 <input class="btn btn-primary btn-block" type="submit" value="Save">  
						  </form>  
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	</div>
  </div>
<!--  End Modal Ingresar Comida                                          -->
<!--  //////////////////////////////////////////////////////////////////////////        -->
<!--   Modal Ingresar Repartidor -->

<div class="modal fade" id="repartidor_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Registrar Menu</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		<form action="../controlador/insertar_repartidor_controlador.php" method="post">                   
  
				
			 
							  <label>Nombre</label>
							  <input type="text"name="nombre_rep"class="form-control"placeholder="Nombre Repartidor...">
							   
							  <label>Apellido</label>
							  <input type="text"name="apellido_rep"class="form-control"placeholder="Apellido...">

							  <label>Telefono</label>
							  <input type="text"name="telefono_rep"class="form-control"placeholder="Telefono...">

					          <label>Numero de Placa</label>
							  <input type="text"name="n_placa"class="form-control"placeholder="Numero de Placa...">

							  <label>Numero de Licencia</label>
							  <input type="text"name="n_licencia"class="form-control"placeholder="Numero de Licencia...">
							  



							
				  
  
							
						
							 <input class="btn btn-primary btn-block" type="submit" value="Save">  
						  </form>  
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	</div>
  </div>
<!--  End Modal Ingresar Repartidor                                         -->
<!--  //////////////////////////////////////////////////////////////////////////        -->

	
	<div class="gtco-section">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 animate-box">
					<h3>Get In Touch</h3>
					<form action="#">
						<div class="row form-group">
							<div class="col-md-12">
								<label class="sr-only" for="name">Name</label>
								<input type="text" id="name" class="form-control" placeholder="Your firstname">
							</div>
							
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label class="sr-only" for="email">Email</label>
								<input type="text" id="email" class="form-control" placeholder="Your email address">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<label class="sr-only" for="message">Message</label>
								<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Write us something"></textarea>
							</div>
						</div>
						<div class="form-group">
							<input type="submit" value="Send Message" class="btn btn-primary">
						</div>

					</form>		
				</div>
				<div class="col-md-5 col-md-push-1 animate-box">
					
					<div class="gtco-contact-info">
						<h3>Contact Information</h3>
						<ul>
							<li class="address">198 West 21th Street, <br> Suite 721 New York NY 10016</li>
							<li class="phone"><a href="tel://1234567920">+ 1235 2355 98</a></li>
							<li class="email"><a href="mailto:info@yoursite.com">info@yoursite.com</a></li>
						
						</ul>
					</div>


				</div>
				</div>
			</div>
		</div>
	</div>

	<footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row row-pb-md">

				

				
			
					<div class="gtco-widget">
						<h3>Get Social</h3>
						<ul class="gtco-social-icons">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>


			</div>

			

		</div>
	</footer>
	<!-- </div> -->

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>
	

	</body>
</html>
